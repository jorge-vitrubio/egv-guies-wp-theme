<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package EGV-Guies
 * @since EGV-Guies 1.0.0
 */

get_header(); ?>

<div class="main-container">
	<!-- <div class="main-grid grid-x">
		<main class="main-content small-order-1 medium-order-2 small-12 medium-8"> -->
	<div class="main-grid grid-x sidebar-left">
		<main class="main-content">
		<?php if ( have_posts() ) : ?>
			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'template-parts/content', '' ); ?>
			<?php endwhile; ?>

			<?php else : ?>
				<?php get_template_part( 'template-parts/content', 'none' ); ?>

			<?php endif; // End have_posts() check. ?>
		</main>
		<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer();
