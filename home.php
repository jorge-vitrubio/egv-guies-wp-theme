<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package EGV-Guies
 * @since EGV-Guies 1.0.0
 */

get_header(); ?>

<div class="main-container">
	<div class="main-grid grid-x">
		<main class="main-content">
				<div class="entry-content">
					<?php get_template_part( 'template-parts/show-egv-guiesmap' ); ?>
				</div>
			</main>
		<?php get_sidebar(); ?>
	</div>
</div>
<?php
get_footer();
