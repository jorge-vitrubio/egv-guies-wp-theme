<?php
/**
 * Basic WooCommerce support
 * For an alternative integration method see WC docs
 * http://docs.woothemes.com/document/third-party-custom-theme-compatibility/
 *
 * @package EGV-Guies
 * @since EGV-Guies 1.0.0
 */

get_header(); ?>

<div class="main-container">
	<!-- <div class="main-grid grid-x">
		<main class="main-content small-order-1 medium-order-2 small-12 medium-8"> -->
	<div class="main-grid grid-x sidebar-left">
		<main class="main-content">
			<!-- content-->
			<?php woocommerce_content(); ?>
		</main>
		<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer();
