<?php
/**
* The template for displaying archive pages
*
* Used to display archive-type pages if nothing more specific matches a query.
* For example, puts together date-based pages if no date.php file exists.
*
* If you'd like to further customize these archive views, you may create a
* new template file for each one. For example, tag.php (Tag archives),
* category.php (Category archives), author.php (Author archives), etc.
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package EGV-Guies
* @since EGV-Guies 1.0.0
*/

get_header(); ?>

<div class="main-container">
	<div class="main-grid grid-x sidebar-left">
		<main class="main-content">
			<?php get_template_part( 'template-parts/show-egv-guiesmap' ); ?>
			<?php if ( is_category() ) : ?>
				<?php if ( have_posts() ) : ?>
		 		  <?php while ( have_posts() ) : the_post(); ?>
						<?php	if ( in_category ( 'text') ) :
								echo '<div class="entry-content">';
								the_content();
								echo	'</div>';
							endif; // End in_category text. ?>
				  <?php endwhile; ?>
				<?php endif; // End have_posts() check. ?>
			<?php endif; // end is_category() check?>
			<?php if ( is_tag() ) :
			    if ( category_description() ) :
						echo '<div class="egv-category-description">';
						echo category_description();
						echo'</div>';
					endif;
			endif; //end is_tag() check ?>
		</main>
	</div>
</div>

<?php get_footer();
