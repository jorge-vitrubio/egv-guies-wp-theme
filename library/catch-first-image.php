<?php
/**
 * Start catch up first image in a post else get the custom logo
 * ----------------------------------------------------------------------------
 * @package EGV-Guies
 */
if ( ! function_exists( 'catch_post_first_image' ) ) :
  function catch_post_first_image() {
    global $post, $posts;
    $first_img = '';
    ob_start();
    ob_end_clean();
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
    $first_img = $matches [1] [0];
    if(!empty($first_img)){
      return $first_img;
    }
    if(empty($first_img)){ //Defines a default image
      // https://developer.wordpress.org/reference/functions/the_custom_logo/
      $custom_logo_image = wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ) , 'full' );
      $first_img = $custom_logo_image[0];
      return $first_img;
    }
  }
endif;
?>
