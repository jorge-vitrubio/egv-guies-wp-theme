<?php
/**
 * Clean up WordPress defaults
 *
 * @package Biofriction
 * @since Biofriction 1.0.0
 */

if ( ! function_exists( 'foundationpress_start_cleanup' ) ) :
	function foundationpress_start_cleanup() {

		// Launching operation cleanup.
		add_action( 'init', 'foundationpress_cleanup_head' );

		// Remove WP version from RSS.
		add_filter( 'the_generator', 'foundationpress_remove_rss_version' );

		// Remove pesky injected css for recent comments widget.
		add_filter( 'wp_head', 'foundationpress_remove_wp_widget_recent_comments_style', 1 );

		// Clean up comment styles in the head.
		add_action( 'wp_head', 'foundationpress_remove_recent_comments_style', 1 );

	}
	add_action( 'after_setup_theme', 'foundationpress_start_cleanup' );
endif;
/**
 * Clean up head.+
 * ----------------------------------------------------------------------------
 */
 // https://webdesignviews.com/clean-up-wordpress-header/
 //
 // https://blacksaildivision.com/how-to-clean-up-wordpress-head-tag

if ( ! function_exists( 'foundationpress_cleanup_head' ) ) :
	function foundationpress_cleanup_head() {

		// EditURI link.
		remove_action( 'wp_head', 'rsd_link' );

		// Category feed links.
		remove_action( 'wp_head', 'feed_links_extra', 3 );

		// Post and comment feed links.
		remove_action( 'wp_head', 'feed_links', 2 );

		// Windows Live Writer.
		remove_action( 'wp_head', 'wlwmanifest_link' );

		// Index link.
		remove_action( 'wp_head', 'index_rel_link' );

		// Previous link.
		remove_action( 'wp_head', 'parent_post_rel_link', 10 );

		// Start link.
		remove_action( 'wp_head', 'start_post_rel_link', 10 );

		// Canonical.
		remove_action( 'wp_head', 'rel_canonical', 10 );

		// Shortlink.
		remove_action( 'wp_head', 'wp_shortlink_wp_head', 10 );

		// Links for adjacent posts.
		remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10 );

		// WP version.
		remove_action( 'wp_head', 'wp_generator' );

		// Emoji detection script.
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );

		// Emoji styles.
		remove_action( 'wp_print_styles', 'print_emoji_styles' );

		// REST API links
		remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );

		// oEmbed discovery links
		remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );

		// disable link header to every page request
		remove_action( 'template_redirect', 'rest_output_link_header', 11 , 0 );

		// oEmbed remove everything
		remove_action( 'wp_head', 'wp_oembed_add_host_js' );
		remove_action( 'rest_api_init', 'wp_oembed_register_route' );
		remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );

		// Remove or setup DNS prefetch links
		remove_action( 'wp_head', 'wp_resource_hints', 2 );

		// Remove Gutenberg default styles
		add_action( 'wp_print_styles', function (): void {
		    wp_dequeue_style('wp-block-library');
		    wp_dequeue_style('wp-block-library-theme');
		});


		//// Remove unnecessary attributes from style tags
		//add_filter('style_loader_tag', function (string $tag, string $handle): string {
		//    // Remove ID attribute
		//    $tag = str_replace("id='${handle}-css'", '', $tag);
		//    // Remove type attribute
		//    $tag = str_replace(" type='text/css'", '', $tag);
		//    // Change ' to " in attributes:
		//    $tag = str_replace('\'', '"', $tag);
		//    // Remove trailing slash
		//    $tag = str_replace(' />', '>', $tag);
		//    // Remove double spaces
		//    return str_replace('  ', '', $tag);
		//}, 10, 2);

	}
endif;

// Remove WP version from RSS.
if ( ! function_exists( 'foundationpress_remove_rss_version' ) ) :
	function foundationpress_remove_rss_version() {
		return '';
	}
endif;

// Remove injected CSS for recent comments widget.
if ( ! function_exists( 'foundationpress_remove_wp_widget_recent_comments_style' ) ) :
	function foundationpress_remove_wp_widget_recent_comments_style() {
		if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
			remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
		}
	}
endif;

// Remove injected CSS from recent comments widget.
if ( ! function_exists( 'foundationpress_remove_recent_comments_style' ) ) :
	function foundationpress_remove_recent_comments_style() {
		global $wp_widget_factory;
		if ( isset( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'] ) ) {
			remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
		}
	}
endif;

// https://crunchify.com/how-to-clean-up-wordpress-header-section-without-any-plugin/
// Remove query strings from all static resources
//if ( ! function_exists( 'crunchify_cleanup_query_string' ) ):
//		function crunchify_cleanup_query_string( $src ){
//			$parts = explode( '?', $src );
//			return $parts[0];
//			}
//		add_filter( 'script_loader_src', 'crunchify_cleanup_query_string', 15, 1 );
//		add_filter( 'style_loader_src', 'crunchify_cleanup_query_string', 15, 1 );
//endif;


// Remove injected CSS and JS from Contact Forms 7.
// https://stackoverflow.com/questions/48923540/remove-contact-form-7-css-and-js-unless-contact-form-7-shortcode-is-used-in-the/48926880#48926880
//if ( ! function_exists( 'function biofriction_contactform_css_js' ) ) :
//		function biofriction_contactform_css_js() {
//		    // global $post;
//		    // if( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'contact-form-7') ) {
//		    //     wp_enqueue_script('contact-form-7');
//		    //      wp_enqueue_style('contact-form-7');
//				//
//		    // }else{
//		    //     wp_dequeue_script( 'contact-form-7' );
//		    //     wp_dequeue_style( 'contact-form-7' );
//		    // }
////
////
//// https://dannyvankooten.com/only-load-contact-form-7-scripts-when-needed/
////
//				$load_scripts = false;
//				if( is_singular() ) {
//					$post = get_post();
//					if( has_shortcode($post->post_content, 'contact-form-7') ) {
//							$load_scripts = true;
//					}
//				}
//				if( ! $load_scripts ) {
//						wp_dequeue_script( 'contact-form-7' );
//						wp_dequeue_style( 'contact-form-7' );
//				}
//		}
//		add_action( 'wp_enqueue_scripts', 'biofriction_contactform_css_js');
//	endif;
