<?php
/**
 * Customize the output of menus for menuguies walker
 *
 * @package EGV-Guies
 * @since EGV-Guies 1.0.0
 */

/**
 * Big thanks to Brett Mason (https://github.com/brettsmason) for the awesome walker
 */

if ( ! class_exists( 'Foundationpress_Top_Bar_Walker' ) ) :
	class Foundationpress_Top_Bar_Walker extends Walker_Nav_Menu {
		function start_lvl( &$output, $depth = 0, $args = array() ) {
			$indent  = str_repeat( "\t", $depth );
			$output .= "\n$indent<ul class=\"horizontal nested menu\">\n";
		}

		// https://awhitepixel.com/blog/wordpress-menu-walkers-tutorial/
		function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
			$output .= "<li class='" .  implode(" ", $item->classes) . "'>";
					$atributes = 'class="guiesbcn"';
					if ($item->url && $item->url != '#') {
            $output .= '<a ' . $atributes . ' href="' . $item->url . '">';
      		} else {
         			$output .= '<span' . $atributes . '>';
       		}

      		$output .= $item->title;

       		if ($item->url && $item->url != '#') {
         			$output .= '</a>';
       		} else {
         			$output .= '</span>';
       		}

		}
	}
endif;
