<?php
/**
 * Modify user roles capabilities
 *
 * Docs: https://wordpress.org/documentation/article/roles-and-capabilities/
 *
 * @package EGV-Guies
 * @since EGV-Guies 1.0.0
 */
// https://wordpress.stackexchange.com/questions/4191/allow-editors-to-edit-menus
// add editor the privilege to edit theme

// get the the role object
$role_object = get_role( 'editor' );

// add $cap capability to this role object
$role_object->add_cap( 'edit_theme_options' );
