<?php
// If a featured image is set, insert into layout and use Interchange
// to select the optimal image size per named media query.
	if ( has_post_thumbnail() ) :?>
		<?php echo get_the_post_thumbnail(); ?>
	<?php else :?>
	<?php	echo '<img src="'. catch_post_first_image() . '>" />';?>
<?php endif;
