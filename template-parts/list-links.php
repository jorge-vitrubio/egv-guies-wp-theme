<?php
/**
 *
 *
 *
 * @package EGV-Guies
 * @since EGV-Guies 1.0.0
 */

?>
<div class="egv_bookmarks">
  <h3 class="egv_title_underline">lista de categorias</h3>

    <?php wp_list_bookmarks(
      // https://developer.wordpress.org/reference/functions/wp_list_bookmarks/
      //
      $args = array (
            'orderby'          => 'name', //Accepts post fields. Default 'name'
            'order'            => 'ASC', // ASC or DESC
            'limit'            => -1, // -1 all, number of post
            'category'         => '', // list category id to display
            'exclude_category' => '', // exclude category by id
            'category_name'    => '', // list category by name
            'hide_invisible'   => 0, // show them = 1|true, hide them = 0|false
            'show_updated'     => 1, // show when they where updated 1 or 0
            'echo'             => 1, // return formated bookmark 1 or 0
      'categorize'       => 1, // show links by categories 1 or 0
      'show_description' => 1, // show or hide description 1 or 0
            'title_li'         => __( 'Bookmarks' ), // What to show before the links appear. Default 'Bookmarks'.
            'title_before'     => '<h2>',
            'title_after'      => '</h2>',
            'category_orderby' => 'name',
            'category_order'   => 'ASC',
            'class'            => 'linkcat',
            'category_before'  => '<li id="%id" class="%class">',
            'category_after'   => '</li>'
      )
    ); ?>

  </div>

