<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package EGV-Guies
 * @since EGV-Guies 1.0.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('cell'); ?>>
	<?php	if ( is_single() ) {
			get_template_part( 'template-parts/show-egv-guiesmap' );
		}	?>
	<main class="entry-content">
		<?php the_content(); ?>
	</main>
	<footer>
	</footer>
</article>
