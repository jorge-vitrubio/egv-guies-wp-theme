<?php
/**
 * Template part for off canvas menu
 *
 * @package EGV-Guies
 * @since EGV-Guies 1.0.0
 */

?>
    <nav class="off-canvas position-right menu-off-canvas-menu " id="<?php foundationpress_menuguies_menu_id(); ?>" data-transition="overlap" data-off-canvas data-auto-focus="false">
      <!-- <header> -->
        <button class="menu-icon close-button" aria-label="<?php _e( 'Close Main Menu', 'foundationpress' ); ?>" type="button" data-close>
          <!-- <span aria-hidden="true">&times;</span> -->
        </button>
      <!-- </header> -->
      <?php foundationpress_menuguies_nav(); ?>
    </nav>
    <div class="off-canvas-content" data-off-canvas-content>
