<?php
/**
 * The default template for displaying wysiwigsummary
 * added with the theme pluggin
 * else will display excerpt
 *
 *
 * @package EGV-Guies
 * @since EGV-Guies 1.0.0
 */

// Retrieves the stored value from the database
$meta_value = get_post_meta( get_the_ID(), 'egv_post-wysiwygsummary', true );
// Checks and displays the retrieved value
if( !empty( $meta_value ) ) {
	?>
	<div class="post-wysiwygsummary">
		<?php echo $meta_value; ?>
	</div>
<?php }
else {
	?>
	<div class="excerpt post-excerpt">
		<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
			<?php echo wp_strip_all_tags( get_the_excerpt(), true ); ?>
		</a>
	</div>
	<?php
}
?>
