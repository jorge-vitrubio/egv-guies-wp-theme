<?php
/**
 * The default template for displaying the map
 * added with the theme pluggin
 * else will display nothing
 *
 *
 * @package EGV-Guies
 * @since EGV-Guies 1.0.0
 */

 //check if the pluggin for the theme is active then do the map
	 // Detect plugin. For use on Front End only.
	 include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	 // check for plugin using plugin name
	 if ( is_plugin_active( 'egv-guies-wp-plugin/egv-guies-theme-plugin-template.php' ) ) {
		 //plugin is activated do
		 egv_archive_map();
	 }
   ?>
