<?php
/**
* The template for displaying the header
*
* Displays all of the head element and everything up until the "container" div.
*
* @package EGV-Guies
* @since EGV-Guies 1.0.0
*/

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
		<header class="site-header-container sticky" data-sticky data-off-canvas-sticky data-margin-top=0>
			<div class="site-header" role="banner">
				  <div class="site-title title-bar" <?php foundationpress_title_bar_responsive_toggle(); ?>>
					<?php if ( has_custom_logo() ) : ?>
						<div class="site-custom-logo"> <?php the_custom_logo(); ?></div>
					<?php	else : ?>
	          <h2 class="site-name">
	            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
	          </h2>
					<?php endif ?>
				  </div>
		  	  <nav class="site-navigation top-bar" id="<?php //foundationpress_menuguies_menu_id(); ?>">
						<div class="top-bar-right">
							<?php foundationpress_top_bar_menu(); ?>
							<button class="menu-icon" aria-label="<?php _e( 'Main Menu', 'foundationpress' ); ?>" type="button" data-toggle="<?php foundationpress_menuguies_menu_id(); ?>"></button>
						</div>
					</nav>
				</div>
				<aside id="" class="egv_title egv_title_underline">
						<h3>
							<?php
							if ( is_home() ){
								echo '<span class="hide">' . get_bloginfo( 'name' )  . ' - ' . get_bloginfo( 'description') . '</span>';
							} elseif ( is_singular() ){
								the_title();
							} elseif ( is_archive()  ) {
								 single_term_title();
							}
							else {
								// the_title( '<h3><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
							}
							?>
						</h3>
				</aside>
		</header>
		<?php get_template_part( 'template-parts/menu-off-canvas' ); ?>
