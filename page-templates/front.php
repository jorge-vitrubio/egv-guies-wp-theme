<?php
/*
Template Name: Front
*/
get_header(); ?>

<div class="main-container grid-container">
	<div class="main-grid grid-x">
		<main class="main-content small-order-1 medium-order-2 small-12">
				<?php get_template_part( 'template-parts/home', 'hero' ); ?>
		</main>
		<secondary class="secondary-content clearfix small-order-2 medium-order-3 small-12">
				<?php get_template_part( 'template-parts/home', 'secondary' ); ?>
		</secondary>
		<?php get_sidebar(); ?>
	</div>
</div>
<?php
get_footer();
