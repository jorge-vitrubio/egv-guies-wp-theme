<?php
/*
Template Name: LightBox Gallery
*/
get_header(); ?>

<?php //get_template_part( 'template-parts/featured-image' ); ?>
<div class="main-container">
	<div class="main-grid sidebar-left">
		<main class="main-content">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php //get_template_part( 'template-parts/content', 'page' ); ?>


<div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit data-box-of-bullets="boxOfBullets">
  <ul class="orbit-container">
    <button class="orbit-previous" aria-label="previous"><span class="show-for-sr">Previous Slide</span>&#9664;</button>
    <button class="orbit-next" aria-label="next"><span class="show-for-sr">Next Slide</span>&#9654;</button>
    <li class="is-active orbit-slide text-center" onclick="openOrbitReveal(this)">
      <img src="http://placehold.it/1000x430" alt="" data-zoom-image="http://placehold.it/2000x860">
      <figcaption class="orbit-caption">1. Space, the final frontier.</figcaption>
    </li>
    <li class="orbit-slide text-center" onclick="openOrbitReveal(this)">
      <img src="http://placehold.it/1000x430" alt="" data-zoom-image="http://placehold.it/2000x860">
      <figcaption class="orbit-caption">2. Lets Rocket!</figcaption>
    </li>
    <li class="orbit-slide text-center" onclick="openOrbitReveal(this)">
      <img src="http://placehold.it/1000x430" alt="" data-zoom-image="http://placehold.it/2000x860">
      <figcaption class="orbit-caption">3. Encapsulating</figcaption>
    </li>
    <li class="orbit-slide text-center" onclick="openOrbitReveal(this)">
      <img src="http://placehold.it/1000x430" alt="" data-zoom-image="http://placehold.it/2000x860">
      <figcaption class="orbit-caption">4. Outta This World</figcaption>
    </li>
  </ul>
  <nav class="row boxOfBullets small-up-2 medium-up-4">
   <button class="is-active column" data-slide="0"><div class="callout small"><img src="http://placehold.it/200x86" alt=""></div><span class="show-for-sr">First slide details.</span><span class="show-for-sr">Current Slide</span></button>
   <button class="column" data-slide="1"><div class="callout small"><img src="http://placehold.it/200x86" alt=""></div><span class="show-for-sr">Second slide details.</span></button>
   <button class="column" data-slide="2"><div class="callout small"><img src="http://placehold.it/200x86" alt=""></div><span class="show-for-sr">Third slide details.</span></button>
   <button class="column" data-slide="3"><div class="callout small"><img src="http://placehold.it/200x86" alt=""></div><span class="show-for-sr">Fourth slide details.</span></button>
 </nav>
</div>
<div class="full reveal" id="orbitReveal" data-reveal>
  <button class="close-button" data-close aria-label="Close reveal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<script type="text/javascript">
function openOrbitReveal(that){
  var $orbitClicked = $(that).closest('div[data-orbit]'),
  $orbitPlaceholder = $('<div class="orbit-placeholder"></div>').insertAfter($orbitClicked).css({'height':$orbitClicked.height()}),
  $orbitReveal = $('#orbitReveal'),
  $zoomImages = $orbitClicked.data('zfPlugin').$slides.find('img[data-zoom-image]');

  $zoomImages.each(function(){
    $this = $(this);
    $this.attr('data-original-image',$this.attr('src')).attr('src',$this.attr('data-zoom-image'));
  });
  Foundation.onImagesLoaded($zoomImages, function(){
    $orbitClicked.data('zfPlugin').$slides.css({'max-height':'none'});
    $orbitClicked.data('zfPlugin').$wrapper.css({'height':'auto'});
    console.log($orbitClicked.data('zfPlugin'));
    //$orbitClicked.data('zfPlugin')._prepareForOrbit();
  });

  $orbitReveal.prepend($orbitClicked);
  $orbitReveal.foundation('open');
  $orbitReveal.off('closed.zf.reveal').on('closed.zf.reveal',function(){
    $zoomImages.each(function(){
      $this = $(this);
      $this.attr('src',$this.attr('data-original-image'));
    });
    $orbitPlaceholder.replaceWith($orbitClicked);
  });
}
</script>


<?php //comments_template(); ?>
<?php endwhile; ?>
</main>
<?php //get_sidebar(); ?>
</div>
</div>
<?php get_footer();
