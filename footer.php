<?php
/**
* The template for displaying the footer
*
* Contains the closing of the "off-canvas-wrap" div and all content after.
*
* @package EGV-Guies
* @since EGV-Guies 1.0.0
*/
?>

<footer class="footer-container">
	<div class="footer-grid-container">
		<div class="footer-grid">
			<?php dynamic_sidebar( 'footer-widgets' ); ?>
		</div>
	</div>
	<div class="credits-footer">
		<div class="credits-footer-grid">
			<div class="">
				<p class="">&copy; <a href="http://elglobusvermell.org" title="el globus vermell - architecture by / for / with people">el globus vermell</a> - content CC 3.0 BY-SA if not otherwhise specified</p>
			</div>
			<div class="">
				<p>web development <a href="http://vitrubio.net" title="web development and design by vitrubio.net">vitrubio.net</a></p>
			</div>
		</div>
	</div>
</footer>

<?php //if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
</div><!-- Close off-canvas content -->
<?php //endif; ?>

<?php wp_footer(); ?>
</body>
</html>
